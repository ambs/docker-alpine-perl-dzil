FROM alpine:3.9


ENV PERL5LIB=/app/local/lib/perl5

RUN apk --no-cache add curl wget perl make ca-certificates zlib libressl \
                       zlib expat gnupg libxml2 libxml2-utils jq          \
                           build-base zlib-dev perl-dev libressl-dev \
                       expat-dev libxml2-dev perl-test-harness-utils  \
    && curl -L https://cpanmin.us | perl - App::cpanminus                \
    && cpanm -n -q App::cpm                                       \
    && rm -rf ~/.cpanm                                                   \
    && cpm install Dist::Zilla Carton

COPY build-perl-deps /usr/bin/
